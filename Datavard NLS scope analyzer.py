# Datavard NLS project analyzer - codenamed Scoper
# Version 1.6
# last update: 15/112/2017
# Author: Peter Gabas (peter.gabas@datavard.com)

import numpy as np
import pandas as pd
import time, os
import datetime
import openpyxl, pprint, warnings
from pathlib import Path

#start timer
#start_time = time.time()

# suppress warnings
warnings.simplefilter("ignore")

# All files should be in /Data/ folder. E.g. /Data/Shell
# Files should be named accordingly:
# QC.xlsx for query collector
# LC.xlsx for lookup collector
# RSLPOPART.xlsx for SPO partitions table
# RSDODSO.xlsx for DSOs table

# Enter customer name here ( = name of the folder)
customer_name = 'Fresenius'

# import HeatMap query statistics from Excel to Pandas dataframe (DF) structure and delete obsolete columns
HM_stats_file = Path("../Data/" + customer_name + "/QC.xlsx").resolve()
print('Importing HeatMap (HM) statistics from file: '+ str(HM_stats_file) + '...')
dataHM = pd.read_excel(HM_stats_file)
del dataHM['Long description']
print('Done importing HM statistics.')

# we don't need type = INFOAREA objects in our analysis. We'll strip the main columns of whitespaces, just to avoid any problems
dataHM = dataHM[dataHM.Type != "INFOAREA"]
dataHM.sort_values(['Parent', 'Object'], inplace=True)
dataHM['Archived'] = 0.0
dataHM["No timecharacteristic found"] = ""
dataHM["Timecharacteristic value missing"] = ""
dataHM["Year"] = 0
dataHM['Object'].str.strip()
dataHM['Type'].str.strip()
dataHM['Parent type'].str.strip()
dataHM['Parent'].str.strip()

OldHMVersion = True

# ************************************************************************************
# THIS PART IS RELEVANT IF SPOS ARE USED IN THE SYSTEM:
# import data from RSLPOPART to Pandas DF - this table contains info about SPOs. Check the excel file first, the names of columns may vary depending on the BW system version
if (os.path.isfile("../Data/" + customer_name + "/RSLPOPART.xlsx")):
    SPO_file = Path("../Data/" + customer_name + "/RSLPOPART.xlsx").resolve()
    print('Importing RSLPOPART table from file: '+ str(SPO_file) + '...')
    dataSPOall = pd.read_excel(SPO_file)
    # new SAP release has whitespaces also in column names for some reason, we need to remove this
    dataSPOall.rename(columns=lambda x: x.strip().replace(" ", "_"), inplace=True)
    print('Done importing RSLPOPART table.')

    # HM does not have the info for parent SPO level, only the partitions
    # the SPO partitions have InfoArea as their parents, this needs to be changed to their SPO (virtual) parents
    # this will rewrite the partitions as lower level and create parent for DSOs, which are missing in HM
    dataSPO = dataSPOall[(dataSPOall['OBJVERS'] == 'A')]

    print('Analyzing Archived data and SPOs from RSLPOPART table. This may take a while...')
    for i, row in dataHM.iterrows():
        if ((str(row['Type']) == "DSO") | (str(row['Type']) == "INFOCUBE") | (str(row['Type']) == "WODS")):
            if ((str(row['Type']) == "WODS")): #means it was a new version of HM and we don't need to get WODS from RSDODSO
                OldHMVersion = False
                #print("New HM version detected - skipping RSDODSO table")
            if str(row['Object']) in dataSPO['PARTPROV'].values:
                parent = dataSPO.loc[(dataSPO['PARTPROV'] == row['Object']), 'LPO'].item() #21/2/17: changed 'Semantically Partitioned InfoProvider' to 'SPO'. 2/3/17: changed again to 'LPO' (Shell)
                parent += '00'
                if (parent in dataHM.values):
                    dataHM.loc[(dataHM['Object'] == str(row['Object'])), 'Parent'] = parent
                    if (str(row['Type']) == "INFOCUBE"):
                        dataHM.loc[(dataHM['Object'] == parent), 'Type'] = 'SPO INFOCUBE'
                        dataHM.loc[(dataHM['Object'] == str(row['Object'])), 'Parent type'] = 'CU'
                    if (str(row['Type']) == "DSO"):
                        dataHM.loc[(dataHM['Object'] == str(row['Object'])), 'Parent type'] = 'DS'
                        dataHM.loc[(dataHM['Object'] == parent), 'Type'] = 'SPO DSO'
                    if (str(row['Type']) == "WODS"):
                        dataHM.loc[(dataHM['Object'] == str(row['Object'])), 'Parent type'] = 'WO'
                        dataHM.loc[(dataHM['Object'] == parent), 'Type'] = 'SPO WODS'
                else:
                    if ((str(row['Type']) == "DSO")):
                        parentInfoArea = row['Parent']
                        new_record = pd.DataFrame([[parent,'SPO DSO',parentInfoArea,'IA','','',0,0,0,0]],columns=['Object','Type', 'Parent','Parent type','Description', 'Extended description', 'Size in GB', 'Number of queries', 'Rows', 'Monetary cost [EUR]'])
                        dataHM = pd.concat([dataHM,new_record], ignore_index=True)
                        dataHM.loc[(dataHM['Object'] == str(row['Object'])), 'Parent'] = parent
                        dataHM.loc[(dataHM['Object'] == str(row['Object'])), 'Parent type'] = 'DS'
                        dataHM.loc[(dataHM['Object'] == parent), 'Type'] = 'SPO DSO'
                    if ((str(row['Type']) == "WODS")):
                        #print("here")
                        parentInfoArea = row['Parent']
                        new_record = pd.DataFrame([[parent,'SPO WODS',parentInfoArea,'IA','','',0,0,0,0]],columns=['Object','Type', 'Parent','Parent type','Description', 'Extended description', 'Size in GB', 'Number of queries', 'Rows', 'Monetary cost [EUR]'])
                        dataHM = pd.concat([dataHM,new_record], ignore_index=True)
                        dataHM.loc[(dataHM['Object'] == str(row['Object'])), 'Parent'] = parent
                        dataHM.loc[(dataHM['Object'] == str(row['Object'])), 'Parent type'] = 'WO'
                        dataHM.loc[(dataHM['Object'] == parent), 'Type'] = 'SPO WODS'
        if (str(row['Type']) == "ARCHIVED_DATA"):
            archived_size = row['Size in GB']
            dataHM.loc[(dataHM['Object'] == row['Parent']), 'Archived'] += archived_size
    print('Done analyzing SPOs from RSLPOPART table.')
# ************************************************************************************

#TO DO: create a counter for parsing yearvalues - of no yearvalues was parsed, the date is in a wrong format (e.g. 2,017)
print('Parsing years from YEAR_VALUES.')
for i, row in dataHM.iterrows():
    if ((str(row['Type']) == "WODS")): #means it was a new version of HM and we don't need to get WODS from RSDODSO
        OldHMVersion = False
    if (str(row['Type']) == "YEAR_VALUES"):
        value = row["Object"]
        characteristic_value = value.rsplit("_",1)[-1]
        if (characteristic_value.isdigit()):
            if (characteristic_value == "0"):
                dataHM.loc[(dataHM["Object"] == row["Parent"]), "Timecharacteristic value missing"] = "X"
                dataHM.loc[(dataHM["Object"] == row["Object"]), "Year"] = 1 #this is because it's hard to work with 0, and 1 is <= lowyear anyway...
            else:
                dataHM.loc[(dataHM["Object"] == row["Object"]), "Year"] = int(characteristic_value)
        elif (characteristic_value == "NONE"):
            dataHM.loc[(dataHM["Object"] == row["Parent"]), "No timecharacteristic found"] = "X"

print('Done parsing years from YEAR_VALUES.')

#get current year and set 'low year' - for query access analysis. Default = currentyear-5
now = datetime.datetime.now()
currentyear = now.year
lowyear = currentyear - 5
print('High year set as: ' + str(currentyear) + ', low year set as: ' + str(lowyear))

# create new columns for number of queries from currentyear to lowyear
col_year = lowyear
for x in range (0, (currentyear-lowyear+1)):
    dataHM[str(col_year)] = 0.0
    col_year += 1

# TO DO: make this in a nicer way
col_year_size = lowyear + 10000
for x in range (0, (currentyear-lowyear+1)):
    dataHM[str(col_year_size)] = 0.0
    col_year_size += 1

#calculate nr. of queries for each YEAR_VALUES and update parent InfoProvider
print('Analyzing HM query statistics. This may take a while...')
for i, row in dataHM.iterrows():
    if row['Year'] > 0:
        # get extended description so we know the characteristic
        ext_descr = row['Extended description']
        dataHM.loc[(dataHM['Object'] == row['Parent']), 'Extended description'] = ext_descr
        if(row['Number of queries'] > 0):
            year = row['Year']
            ifor_val = row['Number of queries']
            # update yearvalues
            if (year >= currentyear):
                if (year == currentyear):
                    year_high = str(currentyear)
                    dataHM.loc[(dataHM['Object'] == row['Parent']), year_high] += ifor_val
            elif (year <= lowyear):
                year_low = str(lowyear) #we dont want query numbers aggregated to 2012 from previous years
                if (year == lowyear):
                    dataHM.loc[(dataHM['Object'] == row['Parent']), year_low] += ifor_val
            else:
                year_tmp = str(year)
                year_tmp = year_tmp[:4]
                dataHM.loc[(dataHM['Object'] == row['Parent']), year_tmp] += ifor_val
        if(row['Size in GB'] > 0):
            year = row['Year']
            ifor_val_size = row['Size in GB']
            if (year >= currentyear):
                year_size = str(10000 + currentyear)
                dataHM.loc[(dataHM['Object'] == row['Parent']), year_size] += ifor_val_size
            elif (year <= lowyear):
                year_size = str(10000 + lowyear)
                dataHM.loc[(dataHM['Object'] == row['Parent']), year_size] += ifor_val_size
            else:
                year_size = str(10000 + year)
                year_size_tmp = year_size[:5]
                dataHM.loc[(dataHM['Object'] == row['Parent']), year_size_tmp] += ifor_val_size
print('Done analyzing HM query statistics.')

# import data from HM Lookup analysis to Pandas DF and analyze lookup statistics
LA_stats_file = Path("../Data/" + customer_name + "/LC.xlsx").resolve()
print('Importing Lookup analysis table from file: '+ str(LA_stats_file) + '...')
dataLAtmp = pd.read_excel(LA_stats_file)
print('Analyzing lookup statistics from HM...')
dataLA = dataLAtmp[['Object', 'DTP source count', 'Lookups count', 'Day avarage load time [s]']].copy()
dataHM = pd.merge(dataHM, dataLA, how='left', on='Object')
print('Done analyzing lookup statistics from HM.')

# ************************************************************************************
# THIS PART IS RELEVANT IF SPOS ARE USED IN THE SYSTEM:
# update parents in SPOs
if (os.path.isfile("../Data/" + customer_name + "/RSLPOPART.xlsx")):
    print('Updating SPO parents with statistic data...')
    for i, row in dataHM.iterrows():
        if ((str(row['Type']) == "DSO") | (str(row['Type']) == "INFOCUBE") | (str(row['Type']) == "WODS")):
            if (str(row['Parent type']) != "IA"):
                parent = row['Parent']
                if(row['Size in GB'] > 0):
                    tmp_size = row['Size in GB']
                    dataHM.loc[(dataHM['Object'] == parent), 'Size in GB'] += tmp_size
                if(row['Number of queries'] > 0):
                    tmp_query = row['Number of queries']
                    parent_query = dataHM[dataHM['Object'] == parent]
                    if (tmp_query > parent_query['Number of queries'].item()):
                        dataHM.loc[(dataHM['Object'] == parent), 'Number of queries'] = tmp_query
                if(row['Rows'] > 0):
                    tmp_row = row['Rows']
                    dataHM.loc[(dataHM['Object'] == parent), 'Rows'] += tmp_row
                if(row['Monetary cost [EUR]'] > 0):
                    tmp_cost = row['Monetary cost [EUR]']
                    dataHM.loc[(dataHM['Object'] == parent), 'Monetary cost [EUR]'] += tmp_cost
                if(row['DTP source count'] > 0):
                    tmp_query = row['DTP source count']
                    parent_query = dataHM[dataHM['Object'] == parent]
                    if (tmp_query > parent_query['DTP source count'].item()):
                        dataHM.loc[(dataHM['Object'] == parent), 'DTP source count'] = tmp_query
                if(row['Lookups count'] > 0):
                    tmp_query = row['Lookups count']
                    dataHM.loc[(dataHM['Object'] == parent), 'Lookups count'] += tmp_query
                if(row['Day avarage load time [s]'] > 0):
                    tmp_query = row['Day avarage load time [s]']
                    parent_query = dataHM[dataHM['Object'] == parent]
                    if (tmp_query > parent_query['Day avarage load time [s]'].item()):
                        dataHM.loc[(dataHM['Object'] == parent), 'Day avarage load time [s]'] = tmp_query
    # update nr. of queries in SPO parents
    print('Updating nr. of queries in SPO parents...')
    dataHM.fillna(value=0)
    tmp_year = currentyear #2017
    for x in range (0, (currentyear-lowyear+1)):
        str_year = str(tmp_year)
        data_size_cell = "1" + str_year
        for i, row in dataHM.iterrows():
            if ((str(row['Type']) == "INFOCUBE") & (str(row['Parent type']) == 'CU')):
                    if((int(row[str_year]) > 0)):
                        object_query = dataHM[dataHM['Object'] == row['Parent']]
                        old_value = object_query[str_year].values
                        value = row[str_year]
                        parent = row['Parent']
                        if (value > old_value):
                            dataHM.loc[(dataHM['Object'] == parent), str_year] = float(value)
                    if(row[data_size_cell] > 0.0):
                        parent = row['Parent']
                        value_data_size = row[data_size_cell]
                        dataHM.loc[(dataHM['Object'] == parent), data_size_cell] += float(value_data_size)
            else:
                if ((str(row['Type']) == "DSO") & (str(row['Parent type']) == 'DS')):
                    if (row[data_size_cell] > 0.0):
                        parent = row['Parent']
                        value_data_size = row[data_size_cell]
                        dataHM.loc[(dataHM['Object'] == parent), data_size_cell] += float(value_data_size)
                if ((str(row['Type']) == "WODS") & (str(row['Parent type']) == 'WO')):
                    if (row[data_size_cell] > 0.0):
                        parent = row['Parent']
                        value_data_size = row[data_size_cell]
                        dataHM.loc[(dataHM['Object'] == parent), data_size_cell] += float(value_data_size)
        tmp_year -= 1
    print('Done updating SPO parents with statistic data...')
# ************************************************************************************

# ************************************************************************************
# NEW HM DOES FIND THE WDOS ITSELF, IF NOT, THIS WILL FIND THEM
# Import RSDODSO table and analyse DSO type
if (OldHMVersion):
    RSDODSO_file = Path("../Data/" + customer_name + "/RSDODSO.xlsx").resolve()
    print('Importing RSDODSO table from file: '+ str(RSDODSO_file) + '...')
    dataRSODSO = pd.read_excel(RSDODSO_file)
    print('Analyzing DSO type...')
    dataRSODSO.rename(columns=lambda x: x.strip().replace(" ", "_"), inplace=True)
    dataRSODSO['OBJVERS'].str.strip()
    dataRSODSO['ODSOTYPE'].str.strip()
    dataRSODSO['ODSOBJECT'].str.strip()
    dataDSO = dataRSODSO[(dataRSODSO['OBJVERS'] == 'A') & (dataRSODSO['ODSOTYPE'] == 'W')]
    dataDSO.rename(columns={'ODSOBJECT': 'Object', 'ODSOTYPE' : 'DSO type'}, inplace=True)
    dataDSOrelevant = dataDSO[['Object', 'DSO type']].copy()
    dataHM = pd.merge(dataHM, dataDSOrelevant, how='left', on='Object')
    print('Done analyzing DSO type.')
# ************************************************************************************

dataHM.sort_values('Object', ascending= True, inplace= True)
print('Exporting final scope to Excel file...')
resultFile = Path("../Data/" + customer_name + "/result.xlsx").resolve()
#header = ['Object','Type','Parent','Parent type','Description','Size in GB','Rows', 'Number of queries']
dataHM.to_excel(str(resultFile), index = False, sheet_name='NLS Project Scope')
print('Done exporting final scope to Excel file. Filepath: ' + str(resultFile))
print('All done!')

#print("---Program executed in: %s seconds ---" % (time.time() - start_time))
