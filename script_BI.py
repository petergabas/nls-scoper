import numpy as np
import pandas as pd
import time, os
import datetime
import openpyxl, pprint, warnings
from pathlib import Path

# suppress warnings
warnings.simplefilter("ignore")

# Enter project_name here ( = name of the folder)
project_name = 'BI'

# import from Excel to Pandas dataframe (DF) structure and delete obsolete columns
data_file = Path("../Data/" + project_name + "/miestnosti.csv").resolve()
print('Importing from file: '+ str(data_file) + '...')
data = pd.read_csv(data_file, delimiter=';', header=None)
print('Done importing.')

data.head()

del data[4]

data.head()

********************


print('Exporting final scope to Excel file...')
resultFile = Path("../Data/" + project_name + "/result.csv").resolve()
#header = ['Object','Type','Parent','Parent type','Description','Size in GB','Rows', 'Number of queries']
data.to_csv(path_or_buf=str(resultFile), header=False)
print('Done exporting final scope to Excel file. Filepath: ' + str(resultFile))
print('All done!')

#print("---Program executed in: %s seconds ---" % (time.time() - start_time))
