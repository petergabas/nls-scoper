#parsing year from YEAR_VALUE - we need this for later
#todo: we have also YEAR_VALUE_0 (time characteristic empty) and YEAR_VALUE_NONE (no time characteristic).
#For _0 => make a new column and flag it. For _NONE -> get max for queries and update all years
print('Parsing years from YEAR_VALUES...')
    def year(row):
     year = ""
     if row['Type'] == "YEAR_VALUES":
         year = row['Object']
         year = year[-4:]
     else:
         year = "NA"
     return year

 dataHM['Year'] = dataHM.apply(year, axis=1)
